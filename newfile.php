<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="utf-8">
    <title>Nimekiri</title>
    <link href="kujundus.css" rel="stylesheet">
</head>
    <body>
    <div id="menu">
        <a href="newfile.php" id="list-page-link">Nimekiri</a> &nbsp;&nbsp;
        <a href="tabel.php" id="add-page-link">Lisa</a> <br><br>
    </div>
    <div id="content">
        <table class="list-table">
            <thead>
            <tr>
                <td class="my-bold my-border">Eesnimi</td>
                <td class="my-bold my-bborder">Perekonnanimi</td>
                <td class="my-bold my-border">Telefonid</td>
            </tr>
            <tr>
                <td>esimene</td>
                <td>teine</td>
            </tr>
            </thead>
        </table>
    </div>
    <?php
    require_once("lib/tpl.php");
    $required = array("firstName", "lastName", "phone");

    $error = false;
    foreach($required as $field) {

        if (isset($_POST["firstName"])) {
            add_todo_item($_POST["firstName"]);
            if (empty($_POST["firstName"])) {
                $error = true;
                //echo "Eesnime väli ei tohi olla tühi";
            }
        }
    }

    if (isset($_POST["lastName"])) {
        add_todo_item($_POST["lastName"]);
        if(empty($_POST["lastName"])){
            $error = true;
            // echo " Perekonnanime väli ei tohi olla tühi";
        }
    }

    if (isset($_POST["phone"])){
        add_todo_item($_POST["phone"]);
        if(empty($_POST["phone"])){
            $error = true;
            // echo " Telefoni väli ei tohi olla tühi";
        }
        if($error){
            echo"Kõik väljad peavad olema täidetud";
        }
    }
    function add_todo_item($firstName){
        if (isset($firstName) && $firstName !== "") {
            $allItems = read_all_items();
            if (!in_array($firstName, $allItems)) {
                file_put_contents("data.txt", $firstName . PHP_EOL, FILE_APPEND);
            }
        }
        $allItems = read_all_items();
        if (!in_array($firstName, $allItems)) {
            file_put_contents("data.txt", $firstName . PHP_EOL, FILE_APPEND);
        }
    }
        function read_all_items(){
            return file("data.txt", FILE_IGNORE_NEW_LINES);
        }

        $allItems = read_all_items();
    ?>
    <ul>
        <?php foreach ($allItems as $item) : ?>
            <li><?php echo $item ?></li>
        <?php endforeach; ?>
    </ul>
    </body>
</html>